const express = require('express');
const router = new express.Router();
const verifyToken = require('../verifyToken');
const NotesController = require('../controllers/notes.controller');

const notesController = new NotesController();

router.get('/', verifyToken, (req, res) => {
  notesController.getAllNotes(req, res);
});

router.post('/', verifyToken, (req, res) => {
  notesController.addNote(req, res);
});

router.get('/:id', verifyToken, (req, res) => {
  notesController.getNoteById(req, res);
});

router.put('/:id', verifyToken, (req, res) => {
  notesController.updateNoteData(req, res);
});

router.patch('/:id', verifyToken, (req, res) => {
  notesController.checkNoteData(req, res);
});

router.delete('/:id', verifyToken, (req, res) => {
  notesController.deleteNotesData(req, res);
});

module.exports = router;
