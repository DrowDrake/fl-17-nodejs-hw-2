const {registerUser, loginUser} = require('../controllers/auth.controller');
const express = require('express');
const router = new express.Router();

router.post('/register', (req, res) => {
  try {
    registerUser(req, res);
  } catch {
    return res.status(500).json({message: 'Oops server is down'});
  }
});

router.post('/login', (req, res) => {
  loginUser(req, res);
});

module.exports = router;
