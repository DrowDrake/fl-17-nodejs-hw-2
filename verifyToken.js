const jwt = require('jsonwebtoken');

const config = process.env;

const verifyToken = (req, res, next) => {
  const token = req.headers['authorization'].split(' ')[1];

  if (!token) {
    return res
        .status(400)
        .json({message: 'A token is required for authentication'});
  }
  try {
    const decoded = jwt.verify(token, config.TOKEN_SECRET);
    req.user = decoded;
  } catch (err) {
    return res.status(400).json({message: 'Invalid Token'});
  }
  return next();
};

module.exports = verifyToken;
