const express = require('express');
const cors = require('cors');
const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');
const YAML = require('yamljs');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const {logsInfo, errorHeandler} = require('./middleware/middlewares');

const userRouter = require('./routes/user');
const notesRouter = require('./routes/notes');
const authRouter = require('./routes/auth');

const app = express();
dotenv.config();
const PORT = process.env.PORT || 8080;
const swaggerDocument = YAML.load('./openapi.yaml');

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Library API',
      version: '1.0.0',
      description: 'A simple Express Library API',
    },
    servers: [
      {
        url: 'http://localhost:8080',
      },
    ],
  },
  apis: ['./routes/*.js'],
};
swaggerJsDoc(options);

const database = process.env.MONGOLAB_URI;
mongoose
    .connect(database, {useUnifiedTopology: true, useNewUrlParser: true})
    .then(() => console.log('Connect to db: Conected'))
    .catch((err) => console.log(err));

app.use(logsInfo);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));
app.use(cors());
app.use(express.json());

app.use('/api/users/me', userRouter);
app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);
app.use(errorHeandler);

app.listen(PORT, console.log(`Server started on port: ${PORT}`));
