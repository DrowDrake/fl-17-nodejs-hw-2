const userRepo = require('../models/user.model');
const mongoose = require('mongoose');

module.exports = class NotesController {
  /**
   * @param {*} req
   * @param {*} res
   *
   */
  async addNote(req, res) {
    const user = await userRepo.findOne({ username: req.user.data.user });

    user.notes.push({ userId: user._id, text: req.body.text, completed: false });
    user
      .save()
      .then(() => {
        res.status(200).json({
          message: 'Success',
        });
      })
      .catch((err) => {
        res.status(500).json({ message: err.message });
      });
  }

  /**
   * @param {*} req
   * @param {*} res
   *
   */
  async getAllNotes(req, res) {
    const user = await userRepo.findOne({ username: req.user.data.user });

    if (user === null) {
      res.status(400).json({ message: 'No such user' });
      return;
    }

    const pagination = new URLSearchParams(req.originalUrl.split('?')[1]);    
    const userNotesFiltered = await userRepo.aggregate([
      { $unwind: '$notes' },
      { $match: { 'notes.userId': { $eq: `${user['_id']}` } } },
      { $skip: +pagination.get('offset') },
      +pagination.get('limit') == 0
        ? { $limit: 20 }
        : { $limit: +pagination.get('limit') },
    ]);
    const map = userNotesFiltered.map((item) => item.notes);
    res.status(200).json({
      offset: +pagination.get('offset'),
      limit: +pagination.get('limit'),
      count: map.length,
      notes: map,
    });
  }
  /**
   * @param {*} req
   * @param {*} res
   *
   */
  getNoteById(req, res) {
    userRepo.findOne(
      { 'notes._id': new mongoose.Types.ObjectId(req.params.id) },
      function (err, item) {
        if (item === null) {
          res.status(400).json({ message: 'Cant find' });
        } else {
          res.json({ note: item?.notes?.id(req.params.id) });
        }
      }
    );
  }
  /**
   * @param {*} req
   * @param {*} res
   *
   */
  updateNoteData(req, res) {
    userRepo.findOne(
      { 'notes._id': new mongoose.Types.ObjectId(req.params.id) },
      function (err, item) {
        const sub = item.notes.id(req.params.id);
        sub.text = req.body.text;
        item.save();
      }
    );
    res.json({
      message: 'Success',
    });
  }
  /**
   * @param {*} req
   * @param {*} res
   *
   */
  checkNoteData(req, res) {
    userRepo.findOne(
      { 'notes._id': new mongoose.Types.ObjectId(req.params.id) },
      function (err, item) {
        const sub = item.notes.id(req.params.id);
        sub.completed = !sub.completed;
        item.save();
      }
    );
    res.json({
      message: 'Success',
    });
  }
  /**
   * @param {*} req
   * @param {*} res
   *
   */
  deleteNotesData(req, res) {
    userRepo.findOne(
      { 'notes._id': new mongoose.Types.ObjectId(req.params.id) },
      function (err, item) {
        item.notes.id(req.params.id).remove();
        item.save();
      }
    );
    res.json({
      message: 'Success',
    });
  }
};
