const bcrypt = require('bcryptjs');
const User = require('../models/user.model');

module.exports = class UserController {
  /**
   * @param {*} req
   * @param {*} res
   *
   */
  getById(req, res) {
    User.findOne(
        {username: req.user.data.user},
        '-notes -password -__v',
        function(err, item) {
          res.json({user: item});
        },
    );
  }
  /**
   * @param {*} req
   * @param {*} res
   *
   */
  async changePassword(req, res) {
    const user = await User.findOne(
        {username: req.user.data.user},
        'password',
    );
    const match = await bcrypt.compare(req.body.oldPassword, user.password);
    if (match) {
      const salt = bcrypt.genSaltSync(10);
      await User.findOneAndUpdate(
          {username: req.user.data.user},
          {password: bcrypt.hashSync(req.body.newPassword, salt)},
          {
            returnOriginal: true,
          },
      );
      res.json({message: 'Success'});
    } else {
      res.status(400).json({message: 'Bad request'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   *
   */
  deleteUser(req, res) {
    User.deleteOne({username: req.user.data.user})
        .then(function() {
          res.json({message: 'Success'});
        })
        .catch(function(err) {
          res.status(500).json({message: err.message});
        });
  }
};
