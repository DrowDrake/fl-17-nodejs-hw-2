const User = require('../models/user.model');
const bcrypt = require('bcryptjs');
const jwt = require('../jwt');

const registerUser = (req, res) => {
  const {username, password} = req.body;

  if (!username || !password) {
    return res.status(400).json({message: 'Password or Login is required'});
  }

  const createdDate = Date.now();
  User.findOne({username: username})
      .then((user) => {
        if (user) {
          console.log('user exists');
          throw new Error('user exists');
        } else {
          const newUser = new User({
            username,
            password,
            createdDate,
          });
          bcrypt.genSalt(10, (err, salt) =>
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              newUser.password = hash;
              newUser
                  .save()
                  .then(() => {
                    res.status(200).json({
                      message: 'Success',
                    });
                  })
                  .catch((err) => {
                    res.status(500).json({message: err.message});
                  });
            }),
          );
        }
      })
      .catch((err) => {
        res.status(500).json({message: err.message});
      });
};

const loginUser = async (req, res) => {
  const {username, password} = req.body;

  if (!username || !password) {
    res.status(400).json({message: 'All input is required'});
  }

  const user = await User.findOne({username: username});

  if (user && (await bcrypt.compare(password, user.password))) {
    const parsedUser = {user: user.username};
    const token = jwt.generateToken(parsedUser);
    res.status(200).json({
      jwt_token: token,
      message: 'Success',
    });
  } else {
    res.status(400).json({message: 'Invalid Credentials'});
  }
};

module.exports = {registerUser, loginUser};
