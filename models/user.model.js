const mongoose = require('mongoose');
const {NotesSchema} = require('./notes.model');
const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: Date,
  notes: [NotesSchema],
});

const User = mongoose.model('Users', UserSchema);
module.exports = User;
