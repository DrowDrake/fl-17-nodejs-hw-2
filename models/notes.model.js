const mongoose = require('mongoose');
const NotesSchema = new mongoose.Schema({
  userId: String,
  text: String,
  completed: Boolean,
  createdDate: {type: Date, default: Date.now},
});

const Notes = mongoose.model('Notes', NotesSchema);
module.exports = {NotesSchema, Notes};
