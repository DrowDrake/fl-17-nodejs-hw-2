const logsInfo = (req, res, next) => {
  console.log(req.path);
  next();
};

const errorHeandler = (error, req, res, next) => {
  res.status(500).json({message: error.message});
  next();
};

module.exports = {logsInfo, errorHeandler};
